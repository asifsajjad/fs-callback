const fs = require('fs');
const path = require('path');

function fileReader(pathUrl, filePathUrl, firstCallback, secondCallback, thirdCallback, fourthCallback) {
    fs.readFile(pathUrl, { encoding: 'utf-8' }, (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            firstCallback(data, filePathUrl, secondCallback, thirdCallback, fourthCallback);
        }
    });
}

// first callback
function convertToUppercaseAndWrite(gotData, filePathUrl, secondCallback, thirdCallback, fourthCallback) {
    const newData = gotData.toUpperCase();
    let uppercasedDataPath= path.join(__dirname,'uppercasedData.txt');
    fs.writeFile(uppercasedDataPath, newData, (err, data) => {
        if (err) {
            console.log(err);
        }
        else {
            fs.writeFile(filePathUrl, 'uppercasedData.txt', (err2, data2) => {
                if (err2) {
                    console.log(err2);
                }
                else {
                    secondCallback(data2, filePathUrl, thirdCallback, fourthCallback);
                }
            });
        }
    });
}

// second callback
function readAndSplit(gotData, filePathUrl, thirdCallback, fourthCallback) {
    let uppercasedDataPath= path.join(__dirname,'uppercasedData.txt');
    fs.readFile(uppercasedDataPath, { encoding: 'utf-8' }, (err3, data3) => {
        if (err3) {
            console.log(err3);
        }
        else {
            const newData = data3.toLowerCase();
            const splitData = newData.split('. ');
            let splitDataPath= path.join(__dirname,'splitData.txt');
            fs.writeFile(splitDataPath, JSON.stringify(splitData), (err4, data4) => {
                if (err4) {
                    console.log(err4);
                }
                else {
                    fs.appendFile(filePathUrl, ' splitData.txt', (err5, data5) => {
                        if (err5) {
                            console.log(err5);
                        }
                        else {
                            thirdCallback(data5, filePathUrl, fourthCallback);
                        }
                    });
                }
            });
        }
    });
}

// third callback 
function sortAndWrite(gotData, filePathUrl, fourthCallback) {
    let splitDataPath= path.join(__dirname,'splitData.txt');
    fs.readFile('./splitData.txt', { encoding: 'utf-8' }, (err6, data6) => {
        if (err6) {
            console.log(err6);
        }
        else {
            let parsedData = JSON.parse(data6);
            let sortedData = parsedData.sort();
            let sortedDataPath= path.join(__dirname,'sortedData.txt');
            fs.writeFile(sortedDataPath, JSON.stringify(sortedData), (err7, data7) => {
                if (err7) {
                    console.log(err7);
                }
                else {
                    fs.appendFile(filePathUrl, ' sortedData.txt', (err10, data10) => {
                        if (err10) {
                            console.log(err10);
                        }
                        else {
                            fourthCallback(filePathUrl);
                        }
                    });
                }
            });
        }
    });
}

//  fourth callback
function viewFilenamesAndDelete(filePathUrl) {
    fs.readFile(filePathUrl, { encoding: 'utf-8' }, (err8, data8) => {
        if (err8) {
            console.log(err8);
        }
        else {
            let filesToDelete = data8.split(" ");
            filesToDelete.map((file) => {
                fs.rm(path.join(__dirname,file), (err9) => {
                    if (err9) {
                        console.log(err9);
                    }
                });
            });
        }
    });
}

function problem2() {
    const pathUrl=path.join(__dirname,'lipsum.txt');
    const filePathUrl= path.join(__dirname,'filenames.txt');
    fileReader(pathUrl, filePathUrl, convertToUppercaseAndWrite, readAndSplit, sortAndWrite, viewFilenamesAndDelete);
}


module.exports= problem2;
