const fs = require('fs');
// const obj = {
//     one: 1,
//     two: 2
// }
// const data = JSON.stringify(obj);

const writeToJson = (fileUrl,data)=>{
   return new Promise((res,rej)=>{
    fs.writeFile(fileUrl, data, (err) => {
        if (err) {
            rej(err);
        }
        else{
            res(data);
            console.log('file written succesfully.');
        }
    });
   }); 
}

async function createJSON(dirUrl,data,callback,callbackArg) {
    fs.mkdir(dirUrl, { recursive: true }, async(err) => {
        if (err) {
            console.log(err);
        }
        else {
            await writeToJson(`${dirUrl}/1.json`,data);
            await writeToJson(`${dirUrl}/2.json`,data);
            await writeToJson(`${dirUrl}/3.json`,data);
            await writeToJson(`${dirUrl}/4.json`,data);  
            callback(callbackArg);          
        }
    });
}

// createJSON(data);

function removeDirAndFiles(dirUrl){

    fs.rm(dirUrl, { recursive: true, force: true }, (err) => {
            if (err) {
                console.log(err);
            }
        });

}
// removeDirAndFiles('./json');

function removeOnlyDirFiles(dirUrl){

        fs.readdir(dirUrl, (err, files) => {
            console.log(files);
            files.map((file) => {
                fs.rm(`${dirUrl}/${file}`, { recursive: true }, (err) => {
                    if (err) {
                        console.log(err);
                    }
                    console.log(`file ${file} removed succesfully.`);

                });
            });
        });
}

module.exports={
    createJSON,
    removeDirAndFiles,
    removeOnlyDirFiles
}
