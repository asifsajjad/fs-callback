const problem1 = require('../problem1');
const dataUrl= '../json';

function testProblem1(dataUrl,data,callback,callbackArg){
   problem1.createJSON(dataUrl,data,callback,callbackArg);
}


const obj = {
    one: 1,
    two: 2
}
const data = JSON.stringify(obj);
testProblem1(dataUrl,data,problem1.removeOnlyDirFiles,dataUrl);